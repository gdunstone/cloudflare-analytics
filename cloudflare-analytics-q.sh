#!/bin/bash
# script modified by Gareth Dunstone
# original metadata below
##      .SYNOPSIS
##      Grafana Dashboard for Cloudflare - Using RestAPI to InfluxDB Script
## 
##      .DESCRIPTION
##      This Script will query Cloudflare GraphQL and send the data directly to InfluxDB, which can be used to present it to Grafana. 
##      The Script and the Grafana Dashboard it is provided as it is, and bear in mind you can not open support Tickets regarding this project. It is a Community Project
##
##      .Notes
##      NAME:  cloudflare-analytics.sh
##      LASTEDIT: 19/03/2021
##      VERSION: 2.0
##      KEYWORDS: Cloudflare, InfluxDB, Grafana
   
##      .Link
##      https://jorgedelacruz.es/
##      https://jorgedelacruz.uk/

TELEGRAF_HOST=${TELEGRAF_HOST:-"telegraf"}
TELEGRAF_PORT=${TELEGRAF_PORT:-"8086"}


# Time variables
# back_seconds=60*60*24*7  # 24 hours
back_seconds=60*60*24  # 24 hours
end_epoch=$(date +'%s')
let start_epoch=$end_epoch-$back_seconds
start_date=$(date --date="@$start_epoch" +'%Y-%m-%d')
end_date=$(date --date="@$end_epoch" +'%Y-%m-%d')

# Payload to query to the new GraphQL (You can always add more variables, or remove the ones you do not need)
PAYLOAD='{ "query":
  "query {
  viewer {
    accounts(filter: {accountTag: $accountTag}) {
      workersInvocationsAdaptive(limit: 500, filter: $filter) {
        sum {
          subrequests
          requests
          errors
        }
        dimensions{
          date
          scriptName
        }
      }
    }
    zones(filter: {zoneTag: $zoneTag}) {
      httpRequests1hGroups(limit:24, filter: $filter, orderBy: [datetime_DESC])   {
        dimensions {
          datetime
        }
        sum {
          bytes
          cachedBytes
          cachedRequests
          contentTypeMap {
            bytes
            requests
            edgeResponseContentTypeName
          }
          countryMap {
            bytes
            requests
            threats
            clientCountryName
          }
          pageViews
          requests
          responseStatusMap {
            requests
            edgeResponseStatus
          }
          threats
          threatPathingMap {
            requests
            threatPathingName
          }
        }
        uniq {
          uniques
        }
      }
    }
  }
}",'
PAYLOAD="$PAYLOAD

  \"variables\": {
    \"accountTag\": \"$CLOUDFLAREACCOUNT\",
    \"zoneTag\": \"$CLOUDFLAREZONE\",
    \"filter\": {
      \"date_geq\": \"$start_date\",
      \"date_leq\": \"$end_date\"
    }
  }
}"

# Cloudflare Analytics. This part will check on your Cloudflare Analytics, extracting the data from the last 24 hours
curl -s -X POST -H "Content-Type: application/json" -H "X-Auth-Email: $CLOUDFLAREEMAIL" -H  "X-Auth-Key: $CLOUDFLAREAPIKEY" --data "$(echo $PAYLOAD)" https://api.cloudflare.com/client/v4/graphql/ | jq
