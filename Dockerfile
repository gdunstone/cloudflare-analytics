FROM alpine:latest

RUN apk add --no-cache bash jq curl tzdata coreutils
COPY cloudflare-analytics.sh /usr/local/bin/cloudflare-analytics.sh
COPY cloudflare-analytics-q.sh /usr/local/bin/cloudflare-analytics-q.sh
RUN chmod +x /usr/local/bin/cloudflare-analytics.sh && \
    echo '0 0 * * * /usr/local/bin/cloudflare-analytics.sh 2>&1' > /etc/crontabs/root
    # echo '* * * * * /usr/local/bin/cloudflare-analytics.sh 2>&1' > /etc/crontabs/root

CMD /usr/local/bin/./cloudflare-analytics.sh && crond -l 2 -f