#!/bin/bash

# script modified by Gareth Dunstone
# original metadata below
##      .SYNOPSIS
##      Grafana Dashboard for Cloudflare - Using RestAPI to InfluxDB Script
## 
##      .DESCRIPTION
##      This Script will query Cloudflare GraphQL and send the data directly to InfluxDB, which can be used to present it to Grafana. 
##      The Script and the Grafana Dashboard it is provided as it is, and bear in mind you can not open support Tickets regarding this project. It is a Community Project
##
##      .Notes
##      NAME:  cloudflare-analytics.sh
##      LASTEDIT: 19/03/2021
##      VERSION: 2.0
##      KEYWORDS: Cloudflare, InfluxDB, Grafana
   
##      .Link
##      https://jorgedelacruz.es/
##      https://jorgedelacruz.uk/

TELEGRAF_HOST=${TELEGRAF_HOST:-"telegraf"}
TELEGRAF_PORT=${TELEGRAF_PORT:-"8086"}

# Time variables
# back_seconds=60*60*24*7  # 24 hours
back_seconds=60*60*24  # 24 hours
end_epoch=$(date +'%s')
let start_epoch=$end_epoch-$back_seconds
start_date=$(date --date="@$start_epoch" +'%Y-%m-%d')
end_date=$(date --date="@$end_epoch" +'%Y-%m-%d')

# Payload to query to the new GraphQL (You can always add more variables, or remove the ones you do not need)
PAYLOAD='{ "query":
  "query {
  viewer {
    accounts(filter: {accountTag: $accountTag}) {
      workersInvocationsAdaptive(limit: 500, filter: $filter) {
        sum {
          subrequests
          requests
          errors
        }
        dimensions{
          date
          scriptName
        }
      }
    }
    zones(filter: {zoneTag: $zoneTag}) {
      httpRequests1hGroups(limit:24, filter: $filter, orderBy: [datetime_DESC])   {
        dimensions {
          datetime
        }
        sum {
          bytes
          cachedBytes
          cachedRequests
          contentTypeMap {
            bytes
            requests
            edgeResponseContentTypeName
          }
          countryMap {
            bytes
            requests
            threats
            clientCountryName
          }
          pageViews
          requests
          responseStatusMap {
            requests
            edgeResponseStatus
          }
          threats
          threatPathingMap {
            requests
            threatPathingName
          }
        }
        uniq {
          uniques
        }
      }
    }
  }
}",'
PAYLOAD="$PAYLOAD

  \"variables\": {
    \"accountTag\": \"$CLOUDFLAREACCOUNT\",
    \"zoneTag\": \"$CLOUDFLAREZONE\",
    \"filter\": {
      \"date_geq\": \"$start_date\",
      \"date_leq\": \"$end_date\"
    }
  }
}"


# Cloudflare Analytics. This part will check on your Cloudflare Analytics, extracting the data from the last 24 hours
cloudflareUrl=$(curl -s -X POST -H "Content-Type: application/json" -H "X-Auth-Email: $CLOUDFLAREEMAIL" -H  "X-Auth-Key: $CLOUDFLAREAPIKEY" --data "$(echo $PAYLOAD)" https://api.cloudflare.com/client/v4/graphql/ 2>&1 -k --silent)

    declare -i workerdays=0
    for requests in $(echo "$cloudflareUrl" | jq -r '.data.viewer.accounts[0].workersInvocationsAdaptive[].sum.requests'); do
      cfWorkerRequestsAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].sum.requests")
      if [[ $cfWorkerRequestsAll = "null" ]]; then
          break
      else
        ## Timestamp
        date=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].dimensions.date")
        cfTimeStamp=`date -d "${date}" '+%s'`
        
        cfScriptName=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].dimensions.scriptName")
        cfWorkersRequests=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].sum.requests // "0"")
        cfWorkersErrors=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].sum.errors // "0"")
        cfWorkersSubRequests=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.accounts[0].workersInvocationsAdaptive[$workerdays].sum.subrequests // "0"")

        metric="cloudflare_analytics_workers,cfZone=$CLOUDFLAREZONE,scriptName=$cfScriptName requests=${cfWorkersRequests}i,errors=${cfWorkersErrors}i,subrequests=${cfWorkersSubRequests}i $cfTimeStamp"
        curl -s -XPOST "$TELEGRAF_HOST:$TELEGRAF_PORT/write?precision=s" --data-binary "$metric"

        workerdays=$workerdays+1
      fi
    done

    declare -i arraydays=0
    for requests in $(echo "$cloudflareUrl" | jq -r '.data.viewer.zones[0].httpRequests1hGroups[].sum.requests'); do
        ## Requests
        cfRequestsAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.requests")
        if [[ $cfRequestsAll = "null" ]]; then
            break
        else
        cfRequestsCached=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.cachedRequests")
        cfRequestsUncached=$(echo "$cfRequestsAll - $cfRequestsCached" | bc)
        
        ## Bandwidth
        cfBandwidthAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.bytes")
        cfBandwidthCached=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.cachedBytes")
        cfBandwidthUncached=$(echo "$cfBandwidthAll - $cfBandwidthCached" | bc)

        ## Threats
        cfThreatsAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.threats")

        ## Pageviews
        cfPageviewsAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.pageViews")

        ## Unique visits
        cfUniquesAll=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].uniq.uniques")

        ## Timestamp
        date=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].dimensions.datetime")
        
        cfTimeStamp=`date -d "${date}" '+%s'`

        metric="cloudflare_analytics,cfZone=$CLOUDFLAREZONE cfRequestsAll=${cfRequestsAll}i,cfRequestsCached=${cfRequestsCached}i,cfRequestsUncached=${cfRequestsUncached}i,cfBandwidthAll=${cfBandwidthAll}i,cfBandwidthCached=${cfBandwidthCached}i,cfBandwidthUncached=${cfBandwidthUncached}i,cfThreatsAll=${cfThreatsAll}i,cfPageviewsAll=${cfPageviewsAll}i,cfUniquesAll=${cfUniquesAll}i $cfTimeStamp"
        curl -s -XPOST "$TELEGRAF_HOST:$TELEGRAF_PORT/write?precision=s" --data-binary "$metric"

        ## requests per content_type
        declare -i arraytype=0
        for requests in $(echo "$cloudflareUrl" | jq -r '.data.viewer.zones[0].httpRequests1hGroups[].sum.contentTypeMap[]'); do
            cfRequestsContentType=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.contentTypeMap[$arraytype].edgeResponseContentTypeName")
            
            if [[ $cfRequestsContentType = "null" ]]; then
                break
            else
              cfRequests=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.contentTypeMap[$arraytype].requests // "0"")
              cfBytes=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.contentTypeMap[$arraytype].bytes // "0"")

              metric="cloudflare_analytics_contenttype,contentType=$cfRequestsContentType requests=${cfRequests}i,bytes=${cfBytes}i $cfTimeStamp"
              curl -s -XPOST "$TELEGRAF_HOST:$TELEGRAF_PORT/write?precision=s" --data-binary "$metric"
              arraytype=$arraytype+1
            fi
        done

        ## Requests per Country
        declare -i arraycountry=0
        for requests in $(echo "$cloudflareUrl" | jq -r '.data.viewer.zones[0].httpRequests1hGroups[].sum.countryMap[]'); do
            cfRequestsCC=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.countryMap[$arraycountry].clientCountryName")
            if [[ $cfRequestsCC = "null" ]]; then
                break
            else
              cfBytes=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.countryMap[$arraycountry].bytes // "0"")
              cfRequests=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.countryMap[$arraycountry].requests // "0"")
              cfThreats=$(echo "$cloudflareUrl" | jq --raw-output ".data.viewer.zones[0].httpRequests1hGroups[$arraydays].sum.countryMap[$arraycountry].threats // "0"")

              metric="cloudflare_analytics_country,country=$cfRequestsCC requests=${cfRequests}i,bytes=${cfBytes}i,threats=${cfThreats}i $cfTimeStamp"
              curl -s -XPOST "$TELEGRAF_HOST:$TELEGRAF_PORT/write?precision=s" --data-binary "$metric"
              arraycountry=$arraycountry+1
            fi
        done          
        
        arraydays=$arraydays+1
        fi
    done  